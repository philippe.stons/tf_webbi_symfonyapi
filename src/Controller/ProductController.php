<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use ProductMapper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/", name="api_product_getAll")
     * @Rest\View()
     * @return ProductDTO[]
     */
    public function getAllAction(EntityManagerInterface $em)
    {
        $item = $em->getRepository(Product::class)->findAll();
        return array_map([ProductMapper::class, 'mapEntityToDTO'], $item);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $productID
     * @Rest\Get(path="/{productID}", requirements={ "productID" : "\d+"})
     * @Rest\View()
     * @return ProductDTO
     */
    public function getByIdAction(Request $request, EntityManagerInterface $em, $productID)
    {
        $item = $em->getRepository(Product::class)->find($productID);
        return ProductMapper::mapEntityToDTO($item);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Product|View|\Symfony\Component\Form\FormInterface
     * 
     * @Rest\Post(path="/")
     * @Rest\View(statusCode="201")
     */
    public function postProductAction(Request $request, EntityManagerInterface $em)
    {
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($product);
            $em->flush();

            return $this->handleView(
                $this->view(['status' => "ok"], Response::HTTP_CREATED)
            );
        }

        return $this->handleView($this->view($form->getErrors()));
    }
}
