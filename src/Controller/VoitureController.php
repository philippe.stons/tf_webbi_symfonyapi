<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Model\VoitureDTO;
use App\Form\VoitureType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use VoitureMapper;

class VoitureController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="/", name="api_Voiture_getAll")
     * @Rest\View()
     * @return VoitureDTO[]
     */
    public function getAllAction(EntityManagerInterface $em)
    {
        $item = $em->getRepository(Voiture::class)->findAll();
        return array_map([VoitureMapper::class, 'mapEntityToDTO'], $item);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $VoitureID
     * @Rest\Get(path="/{VoitureID}", requirements={ "VoitureID" : "\d+"})
     * @Rest\View()
     * @return VoitureDTO
     */
    public function getByIdAction(Request $request, EntityManagerInterface $em, $VoitureID)
    {
        $item = $em->getRepository(Voiture::class)->find($VoitureID);
        return VoitureMapper::mapEntityToDTO($item);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Voiture|View|\Symfony\Component\Form\FormInterface
     * 
     * @Rest\Post(path="/")
     * @Rest\View(statusCode="201")
     */
    public function postVoitureAction(Request $request, EntityManagerInterface $em)
    {
        $Voiture = new Voiture();

        $form = $this->createForm(VoitureType::class, $Voiture);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        if($form->isSubmitted() && $form->isValid())
        {
            $em->persist($Voiture);
            $em->flush();

            return $this->handleView(
                $this->view(['status' => "ok"], Response::HTTP_CREATED)
            );
        }

        return $this->handleView($this->view($form->getErrors()));
    }
}
