<?php

use App\Entity\Voiture;
use App\Model\VoitureDTO;

class VoitureMapper
{
    public static function mapEntityToDTO(Voiture $item)
    {
        $dto = new VoitureDTO();

        $dto->id = $item->getId();
        $dto->name = $item->getName();
        $dto->modele = $item->getModele();
        $dto->annee = $item->getAnnee();
        $dto->puissance = $item->getPuissance();

        return $dto;
    }

    public static function mapDtoToEntity(VoitureDTO $dto)
    {

    }
}