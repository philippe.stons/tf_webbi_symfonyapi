<?php

use App\Entity\Product;
use App\Model\ProductDTO;

class ProductMapper
{
    public static function mapEntityToDTO(Product $item)
    {
        $dto = new ProductDTO();

        $dto->id = $item->getId();
        $dto->name = $item->getName();
        $dto->price = $item->getPrice();

        return $dto;
    }

    public static function mapDtoToEntity(ProductDTO $dto)
    {

    }
}