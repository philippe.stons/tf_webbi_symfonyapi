<?php

namespace App\Model;

use App\Entity\Product;
use JMS\Serializer\Annotation as Serializer;

class ProductDTO 
{
    /**
     * @var integer
     * @Serializer\SerializedName("id")
     */
    public $id;

    /**
     * @var string
     * @Serializer\SerializedName("name")
     */
    public $name;

    /**
     * @var integer
     * @Serializer\SerializedName("price")
     */
    public $price;
}