<?php

namespace App\Model;

use App\Entity\Voiture;
use JMS\Serializer\Annotation as Serializer;

class VoitureDTO 
{
    /**
     * @var integer
     * @Serializer\SerializedName("id")
     */
    public $id;

    /**
     * @var string
     * @Serializer\SerializedName("name")
     */
    public $name;

    /**
     * @var string
     * @Serializer\SerializedName("modele")
     */
    public $modele;

    /**
     * @var integer
     * @Serializer\SerializedName("annee")
     */
    public $annee;

    /**
     * @var integer
     * @Serializer\SerializedName("puissance")
     */
    public $puissance;
}