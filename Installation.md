Créer un projet symfony : 
    - symfony new NOM_DU_PROJET
        ou
    - composer create-project symfony/skeleton NOM_DU_PROJET

Installer les bundles suivants :
composer require jms/serializer-bundle
composer require friendsofsymfony/rest-bundle
composer require sensio/framework-extra-bundle
composer require symfony/validator
composer require symfony/form
composer require symfony/orm-pack
composer require symfony/maker-bundle
composer require handcraftedinthealps/rest-routing-bundle