Créer la database
php bin/console doctrine:database:create 

Préparer la migration
php bin/console make:migration    

Faire la migration
php bin/console doctrine:migrations:migrate

Update la database
php bin/console doctrine:schema:update --dump-sql --force